var arrSimple = [];
var arrBig = new Array(5);
var arrBig2 = new Array(10, "16AB", null);
arrBig2.a = "JS rulez";
arrBig2.b = "JS must die";
arrBig2.tellMe = "NO!";
arrBig2[10] = {};
console.log(typeof arrSimple);
console.log(arrSimple.length);
console.log(arrBig2[1]);
console.log("-------------------------------");
for(var i in arrBig2){
	console.log(arrBig2[i]);
}
console.log("-------------------------------");
Object.defineProperty(arrBig2, 'a', {enumerable: false});
for(var i in arrBig2){
	console.log(arrBig2[i]);
}
console.log("-------------------------------");
Object.defineProperty(arrBig2, 'tellMe', 
		{get: function(){
			if(Math.random() < 0.5){
				return this.a;
			} else {
				return this.b;
			}
		}});
console.log(arrBig2.tellMe);
function tellTruth(){
	alert(arrBig2.tellMe);
}